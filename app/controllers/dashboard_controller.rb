class DashboardController < ApplicationController
  def index
    say_hello
    @movies = Movie.all
  end

  def show
    movieId = params["id"]
    @movie = Movie.find_by(movieId: movieId)
    @ratings = @movie.ratings
  end


end