class Movie < ApplicationRecord
    has_many :ratings, foreign_key: "movieId"

    def self.hits 
        all.select{|movie| movie.average_rating > 4.5}
    end

    def average_rating
        if ratings.count > 0
            average = ratings.pluck(:rating).sum / ratings.count
            average.round(1)
        else
            0
        end
    end
end
