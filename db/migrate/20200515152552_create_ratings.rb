class CreateRatings < ActiveRecord::Migration[6.0]
  def change
    create_table :ratings do |t|
      t.integer :userId
      t.integer :movieId
      t.float :rating
      t.integer :timestamp

      t.timestamps
    end
  end
end
