# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require "csv"
### movielens data ###
# count = 1
# movies = []
# CSV.foreach("public/ml-latest-small/movies.csv", headers: true) do |row|
#     puts count
#     count += 1
#     movies << row.to_h
# end
# Movie.import(movies)

# count = 1
# ratings = []
# CSV.foreach("public/ml-latest-small/ratings.csv", headers: true) do |row|
#     puts count
#     count += 1
#     ratings << row.to_h
# end
# Rating.import(ratings)


# ### cards ###
def create_cards(value)
  cards = []
  card_combos = {"red" => ["diamonds", "hearts"], "black" => ["spades", "clubs" ]}
  
  card_combos.each do |color, suit|
    suit.each do |s|
     Card.create(value: value, color: color, suit: s)
    end
  end
end


values = %w(2 3 4 5 6 7 8 9 10 jack queen king ace)
cards = []

#each value with have four cards assigned to it
values.each do |value|
    cards << create_cards(value)
end

